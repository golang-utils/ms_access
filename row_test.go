package ms_access

import "testing"

func TestDelete(t *testing.T) {
	tests := []struct {
		table    string
		where    map[Field]any
		expected string
	}{
		// 0
		{
			"a",
			map[Field]any{{Name: "b", Type: Type_MEMO}: "huhi"},
			"DELETE * FROM [a] WHERE [b] = 'huhi'",
		},
		// 1
		{
			"a b",
			map[Field]any{{Name: "c", Type: Type_FLOAT}: "3.4"},
			"DELETE * FROM [a b] WHERE [c] = 3.4",
		},
		// 3
		{
			"a b",
			map[Field]any{
				{Name: "c", Type: Type_FLOAT}: "3.4",
				{Name: "d", Type: Type_MEMO}:  "heinz",
			},
			"DELETE * FROM [a b] WHERE [c] = 3.4 AND [d] = 'heinz'",
		},
	}

	for i, test := range tests {
		got := DeleteRow(test.table, test.where)
		want := test.expected

		if got != want {
			t.Errorf("[%v] DeleteRow(%q, %#v) returns\n%s\n\n// expected\n%s\n", i, test.table, test.where, got, want)
		}
	}
}

func TestInsert(t *testing.T) {
	tests := []struct {
		table    string
		values   map[Field]any
		expected string
	}{
		// 0
		{
			"a",
			map[Field]any{{Name: "ab", Type: Type_MEMO}: "huhi"},
			"INSERT INTO [a] ([ab]) VALUES ('huhi');",
		},
		// 1
		{
			"a b",
			map[Field]any{
				{Name: "ab", Type: Type_MEMO}:  "huhi",
				{Name: "xy", Type: Type_FLOAT}: 234,
			},
			"INSERT INTO [a b] ([ab] , [xy]) VALUES ('huhi' , 234);",
		},
	}

	for i, test := range tests {
		got := InsertRow(test.table, test.values)
		want := test.expected

		if got != want {
			t.Errorf("[%v] InsertRow(%q, %#v) returns\n%s\n\n// expected\n%s\n", i, test.table, test.values, got, want)
		}
	}
}

func TestInserts(t *testing.T) {
	tests := []struct {
		table    string
		fields   []Field
		rows     [][]any
		expected string
	}{
		// 0
		{
			"a",
			[]Field{
				{Name: "first", Type: Type_MEMO},
				{Name: "age", Type: Type_INTEGER},
			},
			[][]any{{"brad", 34}, {"carla", 34}},
			"INSERT INTO [a] ([first] , [age]) VALUES (('brad' , 34) , ('carla' , 34));",
		},
		// 1
		{
			"a",
			[]Field{
				{Name: "first", Type: Type_MEMO},
				{Name: "age", Type: Type_INTEGER},
			},
			[][]any{{"brad", 34}},
			"INSERT INTO [a] ([first] , [age]) VALUES (('brad' , 34));",
		},
	}

	for i, test := range tests {
		got, err := InsertRows(test.table, test.fields, test.rows)

		if err != nil {
			t.Errorf("[%v] error: %v", i, err)
			continue
		}

		want := test.expected

		if got != want {
			t.Errorf("[%v] InsertRows(%q, %#v, %#v) returns\n%s\n\n// expected\n%s\n", i, test.table, test.fields, test.rows, got, want)
		}
	}
}

func TestUpdate(t *testing.T) {
	tests := []struct {
		table    string
		values   map[Field]any
		where    map[Field]any
		expected string
	}{
		// 0
		{
			"a",
			map[Field]any{{Name: "ab", Type: Type_MEMO}: "huhi"},
			map[Field]any{{Name: "cd", Type: Type_INTEGER}: 127},
			"UPDATE [a] SET [ab] = 'huhi' WHERE [cd] = 127;",
		},
		// 1
		{
			"a b",
			map[Field]any{
				{Name: "b", Type: Type_MEMO}:    "huhi",
				{Name: "x", Type: Type_VARCHAR}: "ho",
			},
			map[Field]any{
				{Name: "a", Type: Type_MEMO}: "huhi",
				{Name: "g", Type: Type_MEMO}: "huha",
			},
			"UPDATE [a b] SET [b] = 'huhi' , [x] = 'ho' WHERE [a] = 'huhi' AND [g] = 'huha';",
		},
	}

	for i, test := range tests {
		got := UpdateRow(test.table, test.values, test.where)

		want := test.expected

		if got != want {
			t.Errorf("[%v] UpdateRow(%q, %#v, %#v) returns\n%s\n\n// expected\n%s\n", i, test.table, test.values, test.where, got, want)
		}
	}
}
