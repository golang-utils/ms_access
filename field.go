package ms_access

import "fmt"

type fields []Field

func (f fields) Len() int {
	return len(f)
}

func (f fields) Swap(a, b int) {
	f[a], f[b] = f[b], f[a]
}

func (f fields) Less(a, b int) bool {
	if f[a].Name != f[b].Name {
		return f[a].Name < f[b].Name
	}

	return f[a].Type < f[b].Type
}

type Field struct {
	Name string
	Type Type
	Null bool
	Size [2]int
}

func (f Field) Sql() string {
	nullstr := ""
	if !f.Null {
		nullstr = " NOT NULL "
	}

	sizestr := ""

	if f.Size[0] > 0 && f.Size[1] > 0 {
		sizestr = fmt.Sprintf("(%v,%v) ", f.Size[0], f.Size[1])
	}

	return fmt.Sprintf(" [%s] %s%s %s ", f.Name, f.Type, sizestr, nullstr)
}

type Type string

var (
	Type_COUNTER  Type = "COUNTER"
	Type_BYTE     Type = "BYTE"
	Type_SMALLINT Type = "SMALLINT"
	Type_INTEGER  Type = "INTEGER"
	Type_REAL     Type = "REAL"
	Type_FLOAT    Type = "FLOAT"
	// Type_DECIMAL(18,5) FieldType = "DECIMAL(18,5)"
	Type_MONEY            Type = "MONEY"
	Type_VARCHAR          Type = "VARCHAR"
	Type_MEMO             Type = "MEMO"
	Type_DATETIME         Type = "DATETIME"
	Type_BIT              Type = "BIT"
	Type_IMAGE            Type = "IMAGE"
	Type_UNIQUEIDENTIFIER Type = "UNIQUEIDENTIFIER"
)

func AddField(table string, f Field) string {
	return fmt.Sprintf("ALTER TABLE [%s] ADD COLUMN %s;", table, f.Sql())
}

func AlterField(table string, f Field) string {
	return fmt.Sprintf("ALTER TABLE [%s] ALTER COLUMN %s;", table, f.Sql())
}

func DropField(table, field string) string {
	return fmt.Sprintf("ALTER TABLE [%s] DROP COLUMN [%s];", table, field)
}
