# ms_access

This repo provides a command line tool to run common SQL commands to an MS Access (R) database via an ODBC connection.
But it also contains a library to do the same.

It should run on Windows, Linux and Unix.

## Usage

Install the tool with 

```
git clone gitlab.com/golang-utils/ms_access
cd cmd/ms_access
go install .
```

Set the help with:

```sh
ms_access help
```

Here is an example, how to use the library:

get it with

```sh
go get gitlab.com/golang-utils/ms_access@latest
```

Then

```go
package main

import (
  "os"
  "fmt"
  "database/sql"
  "gitlab.com/golang-utils/ms_access"
)

func main() {
  err := run()
  if err != nil {
    fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
    os.Exit(1)
  }

  os.Exit(0)
}

func run() error {
  mydns := "foo"
  mypasswd := "bar"

  conn := ms_access.NewConnection(mydns, mypasswd)
  err := conn.Open()
  if err != nil {
    return fmt.Errorf("could not open ODBC connection %q: %v", dns, err)
	}
	
  defer conn.Close()
  
	row := db.QueryRow("select 1 + 4;")
  var val int
  err = row.Scan(&val)
  if err != nil {
    return err
  }

  fmt.Printf("1 + 4 = %v\n", val)
  return nil
}
```

## Documentation

see https://pkg.go.dev/gitlab.com/golang-utils/ms_access

