package ms_access

import (
	"database/sql"
	"fmt"
)

// NewConnection creates a new ODBC connection
func NewConnection(dsn, passwd string) *Connection {
	return &Connection{
		DSN: dsn,
		PW:  passwd,
	}
}

type Connection struct {
	DSN                    string
	PW                     string
	UID                    string
	DontMaintainConnection bool
}

// Open opens the odbc connection
func (m *Connection) Open() (*sql.DB, error) {
	//db, err := sql.Open("odbc", fmt.Sprintf("DRIVER={Microsoft Access Driver (*.mdb)};DBQ=%s;", dbfilename))
	//connstr := fmt.Sprintf("DRIVER={Microsoft Access Driver (*.mdb)};DSN=%s;Pwd=%s", m.DSN, m.PW)
	connstr := fmt.Sprintf("DSN=%s;Pwd=%s", m.DSN, m.PW)
	if m.DontMaintainConnection {
		connstr += ",Maintainconnection=False"
	}
	db, err := sql.Open("odbc", connstr)
	if err != nil {
		return nil, err
	}
	return db, nil
}
