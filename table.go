package ms_access

import (
	"bytes"
	"fmt"
)

// DropTable removes a table
func DropTable(name string) string {
	return fmt.Sprintf("DROP Table [%s];", name)
}

// CreateTable creates a table
func CreateTable(name string, fields ...Field) string {
	var (
		fieldbf bytes.Buffer
		comma   string
	)

	for _, field := range fields {
		fieldbf.WriteString(comma + field.Sql())
		comma = " , "
	}

	return fmt.Sprintf(`CREATE TABLE [%s] (%s);`, name, fieldbf.String())
}
