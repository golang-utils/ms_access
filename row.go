package ms_access

import (
	"bytes"
	"fmt"
	"sort"
	"strings"
)

// UpdateRow updates a row
func UpdateRow(table string, values map[Field]any, where map[Field]any) string {
	// UPDATE table SET newvalue WHERE criteria;
	var setBf bytes.Buffer

	var valflds fields

	for field := range values {
		valflds = append(valflds, field)
	}

	sort.Sort(valflds)

	var comma string
	//for field, val := range values {
	for _, field := range valflds {
		val := values[field]
		var v string
		switch field.Type {
		case Type_VARCHAR, Type_MEMO, Type_DATETIME:
			v = QuoteString(fmt.Sprintf("%v", val))
		default:
			v = fmt.Sprintf("%v", val)
		}

		setBf.WriteString(comma + fmt.Sprintf("[%s] = %s", field.Name, v))
		comma = " , "
	}

	var whereflds fields

	for field := range where {
		whereflds = append(whereflds, field)
	}

	sort.Sort(whereflds)

	var whereBf bytes.Buffer
	var and string
	//for field, val := range where {
	for _, field := range whereflds {
		val := where[field]
		var v string
		switch field.Type {
		case Type_VARCHAR, Type_MEMO, Type_DATETIME:
			v = fmt.Sprintf("'%v'", val)
		default:
			v = fmt.Sprintf("%v", val)
		}

		whereBf.WriteString(and + fmt.Sprintf("[%s] = %s", field.Name, v))
		and = " AND "
	}

	return fmt.Sprintf("UPDATE [%s] SET %s WHERE %s;", table, setBf.String(), whereBf.String())
}

// DeleteRow deletes a row
func DeleteRow(table string, where map[Field]any) string {
	// DELETE [table.*] FROM table WHERE criteria
	var whereBf bytes.Buffer
	var and string

	var flds fields

	for field := range where {
		flds = append(flds, field)
	}

	sort.Sort(flds)

	//for field, val := range where {
	for _, field := range flds {
		val := where[field]

		var v string
		switch field.Type {
		case Type_VARCHAR, Type_MEMO, Type_DATETIME:
			v = fmt.Sprintf("'%v'", val)
		default:
			v = fmt.Sprintf("%v", val)
		}

		whereBf.WriteString(and + fmt.Sprintf("[%s] = %s", field.Name, v))
		and = " AND "
	}
	return fmt.Sprintf("DELETE * FROM [%s] WHERE %s", table, whereBf.String())
}

func QuoteString(s string) (sql string) {
	s_new := strings.ReplaceAll(s, "'", "' + Chr(39) + '")
	s_new = strings.ReplaceAll(s_new, "\r\n", "' + Chr(13) + Chr(10) + '")
	s_new = strings.ReplaceAll(s_new, "\r", "' + Chr(13) + '")
	s_new = strings.ReplaceAll(s_new, "\n", "' + Chr(10) + '")
	return "'" + s_new + "'"
}

// InsertRow inserts a row
func InsertRow(table string, values map[Field]any) string {
	//insert into [personen] ([Name], [Vorname]) VALUES ('Duck', 'Donald');

	var fieldsBf bytes.Buffer
	var valuesBf bytes.Buffer

	var flds fields

	for field := range values {
		flds = append(flds, field)
	}

	sort.Sort(flds)

	var comma string
	//for field, val := range values {
	for _, field := range flds {
		val := values[field]
		fieldsBf.WriteString(comma + fmt.Sprintf("[%s]", field.Name))

		var v string
		switch field.Type {
		case Type_VARCHAR, Type_MEMO, Type_DATETIME:
			v = QuoteString(fmt.Sprintf("%v", val))
		default:
			v = fmt.Sprintf("%v", val)
		}

		valuesBf.WriteString(comma + v)
		comma = " , "
	}

	return fmt.Sprintf("INSERT INTO [%s] (%s) VALUES (%s);", table, fieldsBf.String(), valuesBf.String())
}

// InsertRows inserts several rows, the order and length of the fields and each row must match
func InsertRows(table string, fields []Field, rows [][]any) (string, error) {
	var fieldsBf bytes.Buffer
	var valuesBf bytes.Buffer

	var comma string

	for _, field := range fields {
		fieldsBf.WriteString(comma + fmt.Sprintf("[%s]", field.Name))
		comma = " , "
	}

	var valuesComma string

	for rowID, row := range rows {
		var rowBF bytes.Buffer
		var rowComma string

		if len(row) != len(fields) {
			return "", fmt.Errorf("row %v has %v columns, but we have %v given fields", rowID+1, len(row), len(fields))
		}

		for colID, val := range row {
			field := fields[colID]
			var v string
			switch field.Type {
			case Type_VARCHAR, Type_MEMO, Type_DATETIME:
				v = QuoteString(fmt.Sprintf("%v", val))
			default:
				v = fmt.Sprintf("%v", val)
			}
			rowBF.WriteString(rowComma + v)

			rowComma = " , "
		}

		valuesBf.WriteString(valuesComma + fmt.Sprintf("(%s)", rowBF.String()))
		valuesComma = " , "
	}

	return fmt.Sprintf("INSERT INTO [%s] (%s) VALUES (%s);", table, fieldsBf.String(), valuesBf.String()), nil
}
