module gitlab.com/golang-utils/ms_access

go 1.23.0

require (
	github.com/alexbrainman/odbc v0.0.0-20240810052813-bcbcb6842ce9
	gitlab.com/golang-utils/updatechecker v0.0.6
)

require golang.org/x/sys v0.0.0-20190916202348-b4ddaad3f8a3 // indirect
