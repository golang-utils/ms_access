package ms_access

import (
	"gitlab.com/golang-utils/updatechecker"
)

func init() {
	updatechecker.PrintVersionCheck("gitlab.com/golang-utils/ms_access")
}

// `Drop Table [~~Kitsch'n Sync];`
/*
		[Auto]                  COUNTER
		[Byte]                  BYTE
		[Integer]               SMALLINT
		[Long]                  INTEGER
		[Single]                REAL
		[Double]                FLOAT
		[Decimal]               DECIMAL(18,5)
		[Currency]              MONEY
		[ShortText]             VARCHAR
		[LongText]              MEMO
		[PlaceHolder1]          MEMO
		[DateTime]              DATETIME
		[YesNo]                 BIT
		[OleObject]             IMAGE
		[ReplicationID]         UNIQUEIDENTIFIER
		[Required]              INTEGER NOT NULL
		[Unicode Compression]   MEMO WITH COMP
		[Indexed]               INTEGER
		CONSTRAINT [PrimaryKey] PRIMARY KEY ([Auto])
		CONSTRAINT [Unique Index] UNIQUE ([Byte],[Integer],[Long])

		CREATE INDEX [Single-Field Index] ON [~~Kitsch'n Sync]([Indexed]);
	  CREATE INDEX [Multi-Field Index] ON [~~Kitsch'n Sync]([Auto],[Required]);
	  CREATE INDEX [IgnoreNulls Index] ON [~~Kitsch'n Sync]([Single],[Double]) WITH IGNORE NULL;
	  CREATE UNIQUE INDEX [Combined Index] ON [~~Kitsch'n Sync]([ShortText],[LongText]) WITH IGNORE NULL;
*/

// https://learn.microsoft.com/en-us/office/client-developer/access/desktop-database-reference/create-table-statement-microsoft-access-sql

/*
	https://learn.microsoft.com/en-us/office/client-developer/access/desktop-database-reference/alter-table-statement-microsoft-access-sql

	ALTER TABLE table {ADD {COLUMN field type[(size)] [NOT NULL] [CONSTRAINT index] | ALTER COLUMN field type[(size)] | CONSTRAINT multifieldindex} | DROP {COLUMN field I CONSTRAINT indexname} }
*/

//sql := `CREATE TABLE [table_y] ([FirstName] FLOAT, [LastName] MEMO NOT NULL);`

//sql := `ALTER TABLE [table_y] {ADD {COLUMN [newField] MEMO NOT NULL}};`
