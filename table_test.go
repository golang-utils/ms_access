package ms_access

import "testing"

/*
passwd: abcd
test-db-access
*/

func TestCreate(t *testing.T) {
	tests := []struct {
		table    string
		fields   []Field
		expected string
	}{
		// 0
		{
			"a",
			[]Field{
				{Name: "b", Type: Type_MEMO},
			},
			"CREATE TABLE [a] ( [b] MEMO  NOT NULL  );",
		},
		// 1
		{
			"a b",
			[]Field{
				{Name: "c", Type: Type_FLOAT, Null: true},
			},
			"CREATE TABLE [a b] ( [c] FLOAT  );",
		},
		// 3
		{
			"a b",
			[]Field{
				{Name: "c", Type: Type_FLOAT, Null: true},
				{Name: "d", Type: Type_INTEGER, Null: false},
			},
			"CREATE TABLE [a b] ( [c] FLOAT   ,  [d] INTEGER  NOT NULL  );",
		},
	}

	for i, test := range tests {
		got := CreateTable(test.table, test.fields...)

		want := test.expected

		if got != want {
			t.Errorf("[%v] CreateTable(%q, %#v) returns\n%s\n\n// expected\n%s\n", i, test.table, test.fields, got, want)
		}
	}
}

func TestDrop(t *testing.T) {
	tests := []struct {
		table    string
		expected string
	}{
		// 0
		{
			"a",
			"DROP Table [a];",
		},
		// 1
	}

	for i, test := range tests {
		got := DropTable(test.table)

		want := test.expected

		if got != want {
			t.Errorf("[%v] DropTable(%q) returns\n%s\n\n// expected\n%s\n", i, test.table, got, want)
		}
	}
}

func TestAlterColumn(t *testing.T) {
	tests := []struct {
		table    string
		field    Field
		expected string
	}{
		// 0
		{
			"a",
			Field{Name: "b", Type: Type_MEMO},
			"ALTER TABLE [a] ALTER COLUMN  [b] MEMO  NOT NULL  ;",
		},
		// 1
		{
			"a b",
			Field{Name: "c", Type: Type_FLOAT, Null: true},
			"ALTER TABLE [a b] ALTER COLUMN  [c] FLOAT  ;",
		},
	}

	for i, test := range tests {
		got := AlterField(test.table, test.field)

		want := test.expected

		if got != want {
			t.Errorf("[%v] AlterField(%q, %#v) returns\n%s\n\n// expected\n%s\n", i, test.table, test.field, got, want)
		}
	}
}

func TestAddColumn(t *testing.T) {
	tests := []struct {
		table    string
		field    Field
		expected string
	}{
		// 0
		{
			"a",
			Field{Name: "b", Type: Type_MEMO},
			"ALTER TABLE [a] ADD COLUMN  [b] MEMO  NOT NULL  ;",
		},
		// 1
		{
			"a b",
			Field{Name: "c", Type: Type_FLOAT, Null: true},
			"ALTER TABLE [a b] ADD COLUMN  [c] FLOAT  ;",
		},
	}

	for i, test := range tests {
		got := AddField(test.table, test.field)

		want := test.expected

		if got != want {
			t.Errorf("[%v] AddField(%q, %#v) returns\n%s\n\n// expected\n%s\n", i, test.table, test.field, got, want)
		}
	}
}

func TestDropColumn(t *testing.T) {
	tests := []struct {
		table    string
		field    string
		expected string
	}{
		// 0
		{
			"a",
			"b",
			"ALTER TABLE [a] DROP COLUMN [b];",
		},
		// 1
		{
			"a b",
			"c",
			"ALTER TABLE [a b] DROP COLUMN [c];",
		},
	}

	for i, test := range tests {
		got := DropField(test.table, test.field)

		want := test.expected

		if got != want {
			t.Errorf("[%v] DropField(%q, %#v) returns\n%s\n\n// expected\n%s\n", i, test.table, test.field, got, want)
		}
	}
}
