package main

import (
	"database/sql"
	"fmt"
	"os"
)

func runExec(db *sql.DB, sqlCode string) error {
	res, err := db.Exec(sqlCode)
	if err != nil {
		return err
	}
	affected, err := res.RowsAffected()
	if err == nil && affected != -1 {
		fmt.Fprint(os.Stdout, fmt.Sprintf("rows affected: %v\n", affected))
	}

	/*
		lastI, err := res.LastInsertId()
		if err == nil && lastI != -1 {

		}
	*/
	return nil
}

func runQuery(db *sql.DB, sqlCode string) error {
	rows, err := db.Query(sqlCode)
	if err != nil {
		return err
	}
	cols, err := rows.Columns()

	if err != nil {
		return fmt.Errorf("error while getting columns for %q: %v", sqlCode, err.Error())
	}

	/*
		ctypes, err := rows.ColumnTypes()

		if err != nil {
			return fmt.Errorf("error while getting column types for %q: %v", err.Error())
		}
	*/

	var rowId int
	defer rows.Close()

	for rows.Next() {
		rowId++

		results := make([]sql.NullString, len(cols))
		resultRefs := make([]any, len(cols))

		for resI := range results {
			resultRefs[resI] = &results[resI]
		}

		err := rows.Scan(resultRefs...)
		if err != nil {
			return fmt.Errorf("Error when scanning row %v: %v ", rowId, err)
		}

		//log.Printf("[%v]\n", rowId)
		fmt.Fprintf(os.Stdout, "[%v]\n", rowId)
		for colID, val := range results {
			/*
				cname := ctypes[colID].DatabaseTypeName()

				if cname == "" {
					cname = fmt.Sprintf("?%v", colID+1)
				}
			*/

			if val.Valid {
				fmt.Fprintf(os.Stdout, "    %s: %q\n", cols[colID], val.String)
			}
		}

		fmt.Fprintln(os.Stdout)
	}
	return nil
}

func runSQL(db *sql.DB, sqlCode string, isquery bool) error {
	if argVerbose.Get() {
		fmt.Println(sqlCode)
	}
	if isquery {
		return runQuery(db, sqlCode)

	}

	return runExec(db, sqlCode)
}
