package main

import (
	"database/sql"
	"fmt"

	"gitlab.com/golang-utils/ms_access"
)

func runUpdate(db *sql.DB, table, fields, values, where string) error {
	flds, err := getFieldDefinitions(fields)
	if err != nil {
		return err
	}
	vals, err := getFieldValueAssocs(values)
	if err != nil {
		return err
	}

	for k, val := range vals {
		for _, f := range flds {
			if k.Name == f.Name {
				delete(vals, k)
				vals[f] = val
			}
		}
	}

	wheres, err := getFieldValueAssocs(where)
	if err != nil {
		return err
	}

	for k, val := range wheres {
		for _, f := range flds {
			if k.Name == f.Name {
				delete(wheres, k)
				wheres[f] = val
			}
		}
	}

	sqlcode := ms_access.UpdateRow(table, vals, wheres)
	if argVerbose.Get() {
		fmt.Println(sqlcode)
	}
	_, err = db.Exec(sqlcode)
	return err
}
