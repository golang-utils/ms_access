package main

import (
	"database/sql"
	"fmt"

	"gitlab.com/golang-utils/ms_access"
)

func runInsert(db *sql.DB, table, fields, values string) error {
	flds, err := getFieldDefinitions(fields)
	if err != nil {
		return err
	}
	vals, err := getFieldValueAssocs(values)
	if err != nil {
		return err
	}

	for k, val := range vals {
		for _, f := range flds {
			if k.Name == f.Name {
				delete(vals, k)
				vals[f] = val
			}
		}
	}
	sqlcode := ms_access.InsertRow(table, vals)
	if argVerbose.Get() {
		fmt.Println(sqlcode)
	}
	_, err = db.Exec(sqlcode)
	return err
}
