package main

import (
	"gitlab.com/golang-utils/config/v2"
)

var (
	cfg = config.New("ms_access", "run sql on an access database via odbc")

	argDSN = cfg.String(
		"dsn",
		"DSN for the connection via odbc",
		config.Required(), config.Shortflag('d'))

	argPassword = cfg.String(
		"passwd",
		"password for the odbc connection",
		config.Required(), config.Shortflag('p'))

	argSQL = cfg.LastString(
		"sql",
		"the sql query to run",
		config.Required())

	argAsQuery = cfg.Bool(
		"query",
		"the given sql is a query (expects a result)",
		config.Shortflag('q'))

	argVerbose = cfg.Bool(
		"verbose",
		"give verbose messages (mostly print the sql before submitting it)",
		config.Shortflag('v'))
)

var (
	cmdCheck = cfg.Command("check", "check the odbc connection").SkipAllBut("dsn", "passwd")

	cmdCreate         = cfg.Command("create", "creates a table").SkipAllBut("dsn", "passwd", "verbose")
	cmdCreateArgTable = cmdCreate.String(
		"table",
		"the table name",
		config.Required(), config.Shortflag('t'))

	cmdCreateArgFields = cmdCreate.String(
		"fields",
		"field definition, e.g. 'firstname VARCHAR NOT NULL'",
		config.Required(), config.Shortflag('f'))
)

var (
	cmdDrop         = cfg.Command("drop", "drops a table").SkipAllBut("dsn", "passwd", "verbose")
	cmdDropArgTable = cmdDrop.String(
		"table",
		"the table name",
		config.Required())
)

var (
	cmdInsert         = cfg.Command("insert", "inserts a row into a table").SkipAllBut("dsn", "passwd", "verbose")
	cmdInsertArgTable = cmdInsert.String(
		"table",
		"the table name",
		config.Required(), config.Shortflag('t'))

	cmdInsertArgFields = cmdInsert.String(
		"fields",
		"field definitions, e.g. 'firstname VARCHAR NOT NULL,...'",
		config.Required(), config.Shortflag('f'))

	cmdInsertArgValues = cmdInsert.String(
		"values",
		"values, e.g. [firstname] = 'Ben', [lastname] = 'Hur'",
		config.Required(), config.Shortflag('x'))
)

var (
	cmdUpdate         = cfg.Command("update", "updates a row in a table").SkipAllBut("dsn", "passwd", "verbose")
	cmdUpdateArgTable = cmdUpdate.String(
		"table",
		"the table name",
		config.Required(), config.Shortflag('t'))

	cmdUpdateArgFields = cmdUpdate.String(
		"fields",
		"field definitions, e.g. 'firstname VARCHAR NOT NULL,...'",
		config.Required(), config.Shortflag('f'))

	cmdUpdateArgValues = cmdUpdate.String(
		"values",
		"values, e.g. (23, 'text')",
		config.Required(), config.Shortflag('x'))

	cmdUpdateArgWhere = cmdUpdate.String(
		"where",
		"where conditions, e.g. [firstname] = 'Ben', [lastname] = 'Hur'",
		config.Required(), config.Shortflag('w'))
)

var (
	cmdDelete         = cfg.Command("delete", "deletes from a table").SkipAllBut("dsn", "passwd", "verbose")
	cmdDeleteArgTable = cmdDelete.String(
		"table",
		"the table name",
		config.Required(), config.Shortflag('t'))

	cmdDeleteArgFields = cmdDelete.String(
		"fields",
		"field definitions, e.g. 'firstname VARCHAR NOT NULL,...'",
		config.Required(), config.Shortflag('f'))

	cmdDeleteArgWhere = cmdDelete.String(
		"where",
		"where conditions, e.g. [firstname] = 'Ben', [lastname] = 'Hur'",
		config.Required(), config.Shortflag('w'))
)

var (
	cmdField         = cfg.Command("field", "dealing with table fields").SkipAllBut("dsn", "passwd", "verbose")
	cmdFieldArgTable = cmdField.String(
		"table",
		"the table name",
		config.Required(), config.Shortflag('t'))

	cmdFieldArgField = cmdField.String(
		"field",
		"field definition, e.g. 'firstname VARCHAR NOT NULL'",
		config.Required(), config.Shortflag('f'))

	cmdFieldArgModification = cmdField.LastString(
		"mod",
		"the modification of the field, valid are 'add', 'drop' or 'alter'",
		config.Required())
)
