package main

import (
	"database/sql"
	"fmt"

	"gitlab.com/golang-utils/ms_access"
)

func runCreate(db *sql.DB, table, fields string) error {
	flds, err := getFieldDefinitions(fields)
	if err != nil {
		return err
	}

	sqlcode := ms_access.CreateTable(table, flds...)
	if argVerbose.Get() {
		fmt.Println(sqlcode)
	}
	_, err = db.Exec(sqlcode)
	return err
}
