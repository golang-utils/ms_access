package main

import (
	"database/sql"
	"fmt"

	"gitlab.com/golang-utils/ms_access"
)

func runDrop(db *sql.DB, table string) error {
	sqlcode := ms_access.DropTable(table)
	if argVerbose.Get() {
		fmt.Println(sqlcode)
	}
	_, err := db.Exec(sqlcode)
	return err
}
