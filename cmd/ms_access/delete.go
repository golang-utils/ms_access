package main

import (
	"database/sql"
	"fmt"

	"gitlab.com/golang-utils/ms_access"
)

func runDelete(db *sql.DB, table, fields, where string) error {
	flds, err := getFieldDefinitions(fields)
	if err != nil {
		return err
	}

	wheres, err := getFieldValueAssocs(where)
	if err != nil {
		return err
	}

	for k, val := range wheres {
		for _, f := range flds {
			if k.Name == f.Name {
				delete(wheres, k)
				wheres[f] = val
			}
		}
	}

	sqlcode := ms_access.DeleteRow(table, wheres)
	if argVerbose.Get() {
		fmt.Println(sqlcode)
	}
	_, err = db.Exec(sqlcode)
	return err
}
