package main

import (
	"database/sql"

	"gitlab.com/golang-utils/ms_access"
)

func getConnection(dns, passwd string) (*sql.DB, error) {
	conn := ms_access.NewConnection(dns, passwd)
	return conn.Open()
}
