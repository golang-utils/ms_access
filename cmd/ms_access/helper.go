package main

import (
	"fmt"
	"strings"

	"gitlab.com/golang-utils/ms_access"
)

func getFieldDefinitions(s string) (fdefs []ms_access.Field, err error) {
	flds := strings.Split(s, ",")

	for _, fld := range flds {
		fd, err := getFieldDefinition(fld)
		if err != nil {
			return nil, err
		}
		fdefs = append(fdefs, fd)
	}
	return fdefs, nil
}

func getFieldDefinition(s string) (f ms_access.Field, err error) {
	flds := strings.Fields(s)
	if len(flds) < 2 {
		err = fmt.Errorf("invalid field definition %q", s)
		return
	}

	f.Name = strings.Trim(strings.TrimSpace(flds[0]), "[]")

	if !regExpNotNull.MatchString(s) {
		//fmt.Println("not not null matching")
		f.Null = true
	}
	str := strings.ToUpper(strings.TrimSpace(flds[1]))

	/*
		COUNTER  FieldType = "COUNTER"
		BYTE     FieldType = "BYTE"
		SMALLINT FieldType = "SMALLINT"
		INTEGER  FieldType = "INTEGER"
		REAL     FieldType = "REAL"
		FLOAT    FieldType = "FLOAT"
		// DECIMAL(18,5) FieldType = "DECIMAL(18,5)"
		MONEY            FieldType = "MONEY"
		VARCHAR          FieldType = "VARCHAR"
		MEMO             FieldType = "MEMO"
		DATETIME         FieldType = "DATETIME"
		BIT              FieldType = "BIT"
		IMAGE            FieldType = "IMAGE"
		UNIQUEIDENTIFIER FieldType = "UNIQUEIDENTIFIER"
	*/

	switch str {
	case "COUNTER":
		f.Type = ms_access.Type_COUNTER
	case "BYTE":
		f.Type = ms_access.Type_BYTE
	case "SMALLINT":
		f.Type = ms_access.Type_SMALLINT
	case "INTEGER":
		f.Type = ms_access.Type_INTEGER
	case "REAL":
		f.Type = ms_access.Type_REAL
	case "FLOAT":
		f.Type = ms_access.Type_FLOAT
	case "MONEY":
		f.Type = ms_access.Type_MONEY
	case "VARCHAR":
		f.Type = ms_access.Type_VARCHAR
	case "MEMO":
		f.Type = ms_access.Type_MEMO
	case "DATETIME":
		f.Type = ms_access.Type_DATETIME
	case "BIT":
		f.Type = ms_access.Type_BIT
	case "IMAGE":
		f.Type = ms_access.Type_IMAGE
	case "UNIQUEIDENTIFIER":
		f.Type = ms_access.Type_UNIQUEIDENTIFIER
	default:
		err = fmt.Errorf("unknown field type %q", str)
		return
	}

	return
}

func getFieldValueAssocs(s string) (m map[ms_access.Field]any, err error) {
	m = map[ms_access.Field]any{}

	arr := strings.Split(s, ",")

	for _, e := range arr {
		f, val, err := getFieldValueAssoc(e)
		if err != nil {
			return m, err
		}
		m[f] = val
	}

	return m, nil
}

func getFieldValueAssoc(s string) (f ms_access.Field, val string, err error) {
	arr := strings.Split(s, "=")
	if len(arr) != 2 {
		err = fmt.Errorf("invalid field value association %q", s)
		return
	}

	f.Name = strings.Trim(strings.TrimSpace(arr[0]), "[]")
	val = strings.Trim(strings.TrimSpace(arr[1]), "'")
	return
}
