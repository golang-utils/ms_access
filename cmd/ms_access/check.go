package main

import (
	"database/sql"
	"fmt"
	"os"
)

func checkConnection(db *sql.DB) error {
	row := db.QueryRow("select 1 + 4 as a;")
	err := row.Err()
	if err != nil {
		return fmt.Errorf("no connection")
	}
	var val int
	err = row.Scan(&val)
	if err != nil {
		return fmt.Errorf("no connection")
	}

	if val != 5 {
		return fmt.Errorf("strange result: %v", val)
	}

	fmt.Fprintf(os.Stdout, "connection is working")
	return nil
}
