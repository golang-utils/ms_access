package main

import (
	"fmt"
	"os"
	"regexp"
)

var regExpNotNull = regexp.MustCompile(`(?i:not\s+null)`)

func main() {
	err := run()
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		os.Exit(1)
	}

	os.Exit(0)
}

func run() error {
	err := cfg.Run()

	if err != nil {
		fmt.Fprintln(os.Stdout, cfg.Usage())
		return err
	}

	db, err := getConnection(argDSN.Get(), argPassword.Get())
	if err != nil {
		return err
	}

	defer db.Close()

	switch cfg.ActiveCommand() {
	case cmdCheck:
		return checkConnection(db)
	case cmdCreate:
		return runCreate(db, cmdCreateArgTable.Get(), cmdCreateArgFields.Get())
	case cmdDrop:
		return runDrop(db, cmdDropArgTable.Get())
	case cmdField:
		return runField(db, cmdFieldArgModification.Get(), cmdFieldArgTable.Get(), cmdFieldArgField.Get())
	case cmdInsert:
		return runInsert(db, cmdInsertArgTable.Get(), cmdInsertArgFields.Get(), cmdInsertArgValues.Get())
	case cmdUpdate:
		return runUpdate(db, cmdUpdateArgTable.Get(), cmdUpdateArgFields.Get(), cmdUpdateArgValues.Get(), cmdUpdateArgWhere.Get())
	case cmdDelete:
		return runDelete(db, cmdDeleteArgTable.Get(), cmdDeleteArgFields.Get(), cmdDeleteArgWhere.Get())
	default:
		return runSQL(db, argSQL.Get(), argAsQuery.Get())
	}
}
