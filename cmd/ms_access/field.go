package main

import (
	"database/sql"
	"fmt"

	"gitlab.com/golang-utils/ms_access"
)

func runField(db *sql.DB, modification, table, field string) error {
	fd, err := getFieldDefinition(field)
	if err != nil {
		return err
	}

	var sqlcode string

	switch modification {
	case "add":
		sqlcode = ms_access.AddField(table, fd)
	case "drop":
		sqlcode = ms_access.DropField(table, fd.Name)
	case "alter":
		sqlcode = ms_access.AlterField(table, fd)
	default:
		return fmt.Errorf("unknown modification: %q", modification)
	}
	if argVerbose.Get() {
		fmt.Println(sqlcode)
	}
	_, err = db.Exec(sqlcode)
	return err
}
